/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.workout.locale;

import com.euler.flint.spider.model.workout.weight.Movement;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author root
 */
@NamedQueries({
    @NamedQuery(name="Language.findAllLanguages" , query ="SELECT l FROM Language l")
})
@Entity
@Table(name="LANG")

public class Language implements Serializable{
    
    public static final int LANGUAGE_ID_SPANISH=3;
    public static final int LANGUAGE_ID_ENGLISH=1;
    public static final int LANGUAGE_ID_GREEK=2;
    
    @Id
    @Column (name="LANGUAGE_ID")
    private int languageId;
    
    @Column(name="LANGUAGE_NAME")
    private String languageName;

    @Column(name="FULLNAME")
    private String fullName;

    public Language() {
        languageId = LANGUAGE_ID_ENGLISH;
    }
    
    public int getLanguageId() {
        return languageId;
    }
    
    
    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
            this.languageName = languageName;
        }

        public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Language)) {
            return false;
        }
        Language other = (Language) object;
        return (languageId == other.getLanguageId());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + this.languageId;
        hash = 61 * hash + Objects.hashCode(this.languageName);
        return hash;
    }

    
}
