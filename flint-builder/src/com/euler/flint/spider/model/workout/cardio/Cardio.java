/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.workout.cardio;

import com.euler.flint.spider.model.workout.ExerciseType;
import java.io.Serializable;
import javax.persistence.Entity;

/**
 *
 * @author agime
 */
@Entity
public class Cardio extends ExerciseType implements Serializable {


    @Override
    public String toString() {
        return "com.euler.flint.spider.model.workout.cardio.Cardio[ id=" + super.getId() + " ]";
    }
    
}
