/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.workout.locale;

import java.io.Serializable;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;


/**
 *
 * @author root
 */

@MappedSuperclass
@Inheritance(strategy = InheritanceType.JOINED)
public class LocaleText implements Serializable{

    public static final String QUERY_GETBYNAME="LocaleText.getByName";
    public static final String PARAM_NAME="name";
    private final LongProperty id = new SimpleLongProperty();
    private final ObjectProperty<Language> language = new SimpleObjectProperty();
    private final StringProperty text = new SimpleStringProperty();

    public LocaleText() {
    }

    public LocaleText( String text, Language language) {
        this.language.set(language);
        this.text.set(text);
    }
    
    public LocaleText(String text) {
        this.text.set(text);
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="FK_LANGUAGE_ID")          
    public Language getLanguage() {
        return language.get();
    }

    public void setLanguage(Language language) {
        this.language.set(language);
    }
    
    @Id
    @TableGenerator(name = "localeGen", table = "EJB_ORDER_SEQUENCE_GENERATOR", pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE", pkColumnValue = "localeId", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "localeGen")      
    public Long getId() {
        return id.get();
    }

    public void setId(Long id) {
        this.id.set(id);
    }

    @Column(name="TEXT", length=20000)
    public String getText() {
        return text.get();
    }

    public void setText(String text) {
       this.text.set(text);
    }
    private StringProperty textProperty() {
        return text;
    }


    private ObjectProperty languageProperty() {
        return language;
    }

   private LongProperty idProperty() {
        return id;
    }
    

}
