/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.workout;

import com.euler.flint.spider.model.workout.locale.Description;
import com.euler.flint.spider.model.workout.locale.LocaleText;
import com.euler.flint.spider.model.workout.locale.Title;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;

/**
 *
 * @author agime
 */
@Entity
public class Exercise implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @TableGenerator(name = "attributeGen", table = "EJB_ORDER_SEQUENCE_GENERATOR", 
                  pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE", 
                  pkColumnValue = "ARTICLE_ID",initialValue=1, allocationSize = 1 )           
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "attributeGen")
    private Long id;        

    @OneToMany
    private List<Title> title = new ArrayList<Title>();
    
    @OneToMany
    private List<Description> description = new ArrayList<Description>();
    
    @OneToOne
    private ExerciseType exercise;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Title> getTitle() {
        return title;
    }

    public void setTitle(List<Title> title) {
        this.title = title;
    }

    public List<Description> getDescription() {
        return description;
    }

    public void setDescription(List<Description> description) {
        this.description = description;
    }



    public ExerciseType getExercise() {
        return exercise;
    }

    public void setExercise(ExerciseType exercise) {
        this.exercise = exercise;
    }
   
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Exercise)) {
            return false;
        }
        Exercise other = (Exercise) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.flint.spider.model.workout.Exercise[ id=" + id + " ]";
    }
    
}
