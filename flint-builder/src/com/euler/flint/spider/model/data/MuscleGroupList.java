/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author root
 */
public class MuscleGroupList {
    
    public static final String plainList ="Abs,Back, Biceps, Chest, Forearm, Glutes, Shoulders, Triceps, Upper Legs, Lower Legs, Cardio";
    private static final List<String> muscleGroups;

    static {
        List<String> aMap = new ArrayList<String>();
        aMap.add("Abs");
        aMap.add("Back");
        aMap.add("Biceps");
        aMap.add("Chest");
        aMap.add("Forearm");
        aMap.add("Glutes");
        aMap.add("Shoulders");
        aMap.add("Triceps");
        aMap.add("Upper Legs");
        aMap.add("Lower Legs");
        aMap.add("Cardio");
        
        muscleGroups = Collections.unmodifiableList(aMap);
    }

    public static List<String> getMuscleGroups() {
        return muscleGroups;
    }
    
}
/***
 *         
 *  
 *      
 * 
 * */