/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author root
 */
public class Equipment {
    
    public static final String plainList = "Bands, Barbell, Bench, Body Only, Dumbbell, Exercise Ball, EZ - Bar, Foam Roll, Kettlebell, Machine - Cardio, Machine - Strength, Others, Pull Bar, Weight Plate";
    private static final List<String> equipmentType;

    static {
        List<String> aMap = new ArrayList<String>();
        aMap.add("Bands");
        aMap.add("Barbell");
        aMap.add("Bench");
        aMap.add("Body Only");
        aMap.add("Dumbbell");
        aMap.add("Exercise Ball");
        aMap.add("EZ - Bar");
        aMap.add("Foam Roll");
        aMap.add("Kettlebell");
        aMap.add("Machine - Cardio");
        aMap.add("Machine - Strength");
        aMap.add("Other");
        aMap.add("Pull Bar");
        aMap.add("Weight Plate");
        
        equipmentType = Collections.unmodifiableList(aMap);
    }

    public static List<String> getEquipmentTypes() {
        return equipmentType;
    }
    
}
/***
 *  
        
 * 
 * */