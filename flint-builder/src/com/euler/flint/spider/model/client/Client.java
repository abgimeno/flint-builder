/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.client;

import com.euler.flint.spider.model.gym.Gym;
import com.euler.flint.spider.model.workout.Workout;
import java.io.Serializable;
import java.util.List;
import javafx.beans.property.ListProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;

/**
 *
 * @author agime
 */
@Entity
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private final LongProperty id = new SimpleLongProperty();
    private final StringProperty username = new SimpleStringProperty();        
    private final StringProperty password = new SimpleStringProperty();    
    private final StringProperty appKey = new SimpleStringProperty();        
    private final ObjectProperty<Gym> gym = new SimpleObjectProperty<Gym>();    
    private final ListProperty<Workout> workoutHistory = new SimpleListProperty();    
    private final ObjectProperty<Workout> activeWorkout = new SimpleObjectProperty<Workout>();  
    

    @Id
    @TableGenerator(name = "clientGen", table = "EJB_ORDER_SEQUENCE_GENERATOR", pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE", pkColumnValue = "clientId", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "clientGen")
    public Long getId() {
        return id.get();
    }

    public void setId(Long id) {
        this.id.set(id);
    }
            
    @Column
    public String getUsername() {
        return username.get();
    }

    public void setUsername(String username) {
        this.username.set(username);
    }
    
    @Column    
    public String getPassword() {
        return password.get();
    }

    public void setPassword(String password) {
        this.password.set(password);
    }
    
    @Column
    public String getAppKey() {
        return appKey.get();
    }
    
    public void setAppKey(String appKey) {
        this.appKey.set(appKey);
    }
    
    @ManyToOne
    public Gym getGym() {
        return gym.get();
    }

    public void setGym(Gym gym) {
        this.gym.set(gym);
    }

    @OneToMany
    public List<Workout> getWorkoutHistory() {
        return workoutHistory.get();
    }

    public void setWorkoutHistory(List<Workout> workoutHistory) {
        ListProperty items = new SimpleListProperty(FXCollections.observableArrayList());
        items.addAll(workoutHistory);      
        this.workoutHistory.set(items);
    }

    @OneToOne
    public Workout getActiveWorkout() {
        return activeWorkout.get();
    }

    public void setActiveWorkout(Workout activeWorkout) {
        this.activeWorkout.set(activeWorkout);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.flint.spider.model.client.Client[ id=" + id + " ]";
    }
}
