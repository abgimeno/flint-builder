/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.euler.flint;

import com.euler.flint.bean.MovementBean;
import com.euler.flint.fx.MovementColumnBuilder;
import com.euler.flint.fx.MultiLanguageTableCell;
import com.euler.flint.spider.model.workout.locale.Language;
import com.euler.flint.spider.model.workout.locale.LocaleText;
import com.euler.flint.spider.model.workout.locale.Title;
import com.euler.flint.spider.model.workout.weight.Movement;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

/**
 *
 * @author abrahamgimeno
 */
public class StartScreenController implements Initializable {
    
    
    private static final String PERSISTENCE_UNIT= "flint-spiderPU";
    private static final String HSLDB= "flint-hsqlPU";
    
    private MovementBean movBean;
    private MovementBean movHsqlBean;
    
    @FXML //  fx:id="dayTabPane"
    private TabPane dayTabPane; // Value injected by FXMLLoader

    @FXML //  fx:id="keyword"
    private TextField keyword; // Value injected by FXMLLoader

    @FXML //  fx:id="moveTable"
    private TableView<Movement> moveTable; // Value injected by FXMLLoader

    private MovementColumnBuilder columnBuilder = new MovementColumnBuilder();
    
    protected final Callback<TableColumn<Movement, Long>, TableCell<Movement, Long>> longFactory;
    //protected final Callback<TableColumn<Movement, String>, TableCell<Movement, String>> stringFactory;
    protected Callback<TableColumn<Movement, String>, TableCell<Movement, List<? extends LocaleText>>> multiLang;
    
    public StartScreenController() {
        longFactory = new Callback<TableColumn<Movement, Long>, TableCell<Movement, Long>>() {
            @Override
            public TableCell<Movement, Long> call(TableColumn<Movement, Long> p) {
                return new TableCell<Movement, Long>();
            }
        };
        multiLang = new Callback<TableColumn<Movement, String>, TableCell<Movement, List<? extends LocaleText>>>() {
            @Override
            public TableCell<Movement, List<? extends LocaleText>> call(TableColumn<Movement, String> p) {
                return new MultiLanguageTableCell(new Language());
            }
        };            
        try {
            movBean = new MovementBean(PERSISTENCE_UNIT);
            movHsqlBean = new MovementBean(HSLDB);
        } catch (IOException ex) {
            Logger.getLogger(StartScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        assert dayTabPane != null : "fx:id=\"dayTabPane\" was not injected: check your FXML file 'StartScreen.fxml'.";
        assert keyword != null : "fx:id=\"keyword\" was not injected: check your FXML file 'StartScreen.fxml'.";
        assert moveTable != null : "fx:id=\"moveTable\" was not injected: check your FXML file 'StartScreen.fxml'.";    
        
        TableColumn<Movement, Long> col = createColumn("id", longFactory);                   
        TableColumn<Movement, List<? extends LocaleText>> title = createColumnLocaleTextList("title", multiLang);
        TableColumn<Movement, List<? extends LocaleText>> description = createColumnLocaleTextList("description", multiLang);
        TableColumn<Movement, List<? extends LocaleText>> equipment = createColumnLocaleTextList("equipment", multiLang);
        TableColumn<Movement, List<? extends LocaleText>> muscleGroup = createColumnLocaleTextList("muscleGroup", multiLang);
        TableColumn<Movement, List<? extends LocaleText>> difficulty = createColumnLocaleTextList("difficulty", multiLang);
        
        ObservableList movements = FXCollections.<Movement>observableArrayList();
        List<Movement> listOfMovements = movBean.getAllMovements();
        movements.addAll(listOfMovements.subList(0, 600));        
        moveTable.getColumns().add(col);
        moveTable.getColumns().add(title);
        moveTable.getColumns().add(description);
        moveTable.getColumns().add(equipment);
        moveTable.getColumns().add(muscleGroup);
        moveTable.getColumns().add(difficulty);
        
        moveTable.setItems(movements);
    }    

    public TableColumn createColumn(String propertyName, Callback callback) {
        TableColumn<Movement, Long> col = new TableColumn<>();
        col.setText(propertyName);
        col.setCellValueFactory(new PropertyValueFactory<Movement, Long>(propertyName));
        col.setCellFactory(callback);

        if (callback != null){
            col.setCellFactory(callback);
        }
        col.setId(propertyName);
        
        return col;
    }  
    
    public TableColumn<Movement, List<? extends LocaleText>> createColumnLocaleTextList(String propertyName, Callback callback) {
        TableColumn<Movement, List<? extends LocaleText>> col = new TableColumn<>();
        col.setText(propertyName);
        col.setCellValueFactory(new PropertyValueFactory<Movement, List<? extends LocaleText>>(propertyName));
        col.setCellFactory(callback);
        
        if (callback != null){
            col.setCellFactory(callback);
        }
        col.setId(propertyName);
        
        return col;
    }
    
    private void saveValuesFromOldDB(List<Movement> listOfMovements){
        for(Movement m: listOfMovements){
            if(m.getTitle().isEmpty()){
                System.out.println("Saving " + m.getId());
            } else {            
                System.out.println("Saving " + m.getTitle().get(0).getText());
            }
            movHsqlBean.merge(m);
        }
        System.out.println(listOfMovements.size()+ " movements found on db");        
    }
}
