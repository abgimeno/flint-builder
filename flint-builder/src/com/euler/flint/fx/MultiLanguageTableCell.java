/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.fx;

import com.euler.flint.spider.model.workout.locale.Language;
import com.euler.flint.spider.model.workout.locale.LocaleText;
import com.euler.flint.spider.model.workout.weight.Movement;
import java.util.List;
import javafx.scene.control.TableCell;

/**
 *
 * @author abrahamgimeno
 */
public class MultiLanguageTableCell extends TableCell<Movement, List<? extends LocaleText>> {

    private final Language language;

    public MultiLanguageTableCell(Language language) {
        this.language = language;
    }

    @Override
    protected void updateItem(List<? extends LocaleText> texts, boolean empty) {
        if(!empty){
            for(LocaleText lt: texts){
                if(lt.getLanguage().equals(language)){                                        
                        setText(lt.getText());                  
                }
            }
        }
        super.updateItem(texts, empty);
                 //To change body of generated methods, choose Tools | Templates.

    }

}
